name := "AuthenticationService"

version := "0.2.0"

scalaVersion := "2.12.2"

// Dependencies
libraryDependencies ++= Seq(
  "org.typelevel" %% "cats" % "0.9.0",
  "org.tpolecat" %% "doobie-core-cats" % "0.4.1",
  "org.tpolecat" %% "doobie-scalatest-cats" % "0.4.1",
  "mysql" % "mysql-connector-java" % "5.1.35",
  "com.roundeights" %% "hasher" % "1.2.0",
  "org.scalatest" %% "scalatest" % "3.0.1" % "test",
  "org.scalacheck" %% "scalacheck" % "1.13.4" % "test",
  "org.scalamock" %% "scalamock-scalatest-support" % "3.6.0" % "test"
)

// Sonar properties to push coverage and style report
sonarProperties ++= Map(
  "sonar.host.url" -> "http://localhost:9000",
  "sonar.scoverage.reportPath" -> "target/scala-2.12/scoverage-report/scoverage.xml"
)

// Assemble settings to publish fat jar with deps
assemblyMergeStrategy in assembly := {
 case PathList("META-INF", xs @ _*) => MergeStrategy.discard
 case x => MergeStrategy.first
}

lazy val commonSettings = Seq(
  version := "0.2.0",
  organization := "com.petsupplies",
  scalaVersion := "2.12.2",
  test in assembly := {}
)

lazy val authenticationservice = (project in file(".")).
  settings(commonSettings: _*)

// Neccessary to publish the assembled jar to the target repository
artifact in (Compile, assembly) := {
  val art = (artifact in (Compile, assembly)).value
  art.copy(`classifier` = Some("assembly"))
}

addArtifact(artifact in (Compile, assembly), assembly)

enablePlugins(SonarRunnerPlugin)
