package com.petsupplies.authenticationservice.util.crypto

import org.scalacheck.Properties
import org.scalacheck.Prop.forAll
import org.scalacheck.Gen

object Sha256Prop extends Properties("Sha256") {
  property("length should be 64") = forAll(Gen.alphaNumStr) { Sha256.hash(_).length == 64 }
}
