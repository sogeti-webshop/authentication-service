package com.petsupplies.authenticationservice.util.crypto

import org.scalatest.{FlatSpec, Matchers}

class Sha256Spec extends FlatSpec with Matchers {
  "Sha256 hasher" should "hash a given string" in {
    assert(Sha256.hash("test") == "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08")
  }
}
