package com.petsupplies.authenticationservice.util.validation

import cats.data.Validated.{Invalid, Valid}
import org.scalatest.{FlatSpec, Matchers}

class ValidationSpec extends FlatSpec with Matchers {
  "Regex validation" should "succeed if value matches pattern" in {
    Validation.regex("12323", "[0-9]{5}$")("message") match {
      case Valid(x) => assert(x === "12323")
      case Invalid(_) => fail("The value matches the pattern and should have produced a valid validation")
    }
  }

  it should "fail if value doesn't matches pattern" in {
    Validation.regex("123230", "[0-9]{5}$")("message") match {
      case Valid(x) => fail("The value doesn't matches the pattern and should not have successfully validated")
      case Invalid(x) => x.head match {
        case ValidationFailed(validationMessage) => assert(validationMessage === "message")
      }
    }
  }
}
