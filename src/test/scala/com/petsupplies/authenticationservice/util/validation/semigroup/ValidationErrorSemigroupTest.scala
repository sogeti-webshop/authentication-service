package com.petsupplies.authenticationservice.util.validation.semigroup

import com.petsupplies.authenticationservice.util.validation.ValidationError
import org.scalacheck.Properties
import org.scalacheck.Prop.forAll
import com.petsupplies.authenticationservice.util.validation.semigroup.ValidationErrorSemigroup._
import cats.syntax.semigroup._
import com.petsupplies.authenticationservice.util.validation.generators.ValidationErrorGen.validationErrorGen

object ValidationErrorSemigroupTest extends Properties("ValidationErrorSemigroup") {
  property("associativity") = forAll(validationErrorGen, validationErrorGen, validationErrorGen) {
    (value1: ValidationError, value2: ValidationError, value3: ValidationError) => {
      ((value1 |+| value2) |+| value3) == (value1 |+| (value2 |+| value3))
    }
  }
}
