package com.petsupplies.authenticationservice.util.validation

import cats.data.Validated.{Invalid, Valid}
import org.scalacheck.Properties
import org.scalacheck.Prop.forAll
import org.scalacheck.Gen
import org.scalatest.Matchers

object ValidationProp extends Properties("Validation") with Matchers {
  property("MinLength should succeed if value is same or higher than minLength") =
      forAll(Gen.choose(0, 100), Gen.alphaNumStr, Gen.alphaNumStr) {
        (minLength: Int, value: String, message: String) => {
          val result = Validation.minLength(value, minLength)(message)

          if (minLength <= value.length) {
            result match {
              case Valid(output) => output == value
              case Invalid(_) => fail("Validation should have resulted in valid because minLength is smaller than length of input")
            }
          }
          else {
            result match {
              case Valid(_) => fail("Validation should have resulted in invalid because minLength is larger than length of input")
              case Invalid(x) => x.head match {
                case ValidationFailed(validationMessage) => validationMessage == message
              }
            }
          }
        }
      }

  property("In between length should succeed when value is between provided ranges") =
      forAll(Gen.choose(0, 50), Gen.choose(50, 100), Gen.alphaNumStr, Gen.alphaNumStr) {
        (minLength: Int, maxLength: Int, value: String, message: String) => {
          val result = Validation.inBetweenLength(value, minLength, maxLength)(message)
          val stringLength = value.length

          if (stringLength >= minLength && stringLength <= maxLength)
            result match {
              case Valid(output) => output == value
              case Invalid(_) => fail("Validation should have resulted in valid because its in between range")
            }
          else
            result match {
              case Valid(_) => fail("Validation should have resulted in invalid because its not in between range")
              case Invalid(x) => x.head match {
                case ValidationFailed(validationMessage) => validationMessage == message
              }
            }
        }
      }

  property("Non empty should be valid when string is not empty") =
      forAll(Gen.alphaNumStr, Gen.alphaNumStr) {
        (value: String, message: String) => {
          val result = Validation.nonEmpty(value)(message)

          if (value.length > 0)
            result match {
              case Valid(output) => output == value
              case Invalid(_) => fail("Validation should have resulted in valid because its not empty")
            }
          else {
            result match {
              case Valid(_) => fail("Validation should have resulted in invalid because its empty")
              case Invalid(x) => x.head match {
                case ValidationFailed(validationMessage) => validationMessage == message
              }
            }
          }
        }
      }
}
