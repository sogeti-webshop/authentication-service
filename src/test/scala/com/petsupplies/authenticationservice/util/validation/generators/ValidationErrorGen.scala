package com.petsupplies.authenticationservice.util.validation.generators

import com.petsupplies.authenticationservice.util.validation.{ValidationError, ValidationFailed}
import org.scalacheck.Gen

object ValidationErrorGen {
  implicit val validationErrorGen: Gen[ValidationError] = for {
    message <- Gen.alphaNumStr
  } yield ValidationFailed(message)
}
