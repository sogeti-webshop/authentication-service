package com.petsupplies.authenticationservice.interpreter

import java.time.Instant

import cats.{Id, catsInstancesForId, ~>}
import com.petsupplies.authenticationservice.dsl.{Auth, AuthAction}
import com.petsupplies.authenticationservice.persistence.AuthRepository
import com.petsupplies.authenticationservice.types.AccessToken
import com.petsupplies.authenticationservice.types.Client.Client
import com.petsupplies.authenticationservice.types.PersonalData.PersonalData
import com.petsupplies.authenticationservice.types.User.User
import fs2.Task
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers, OneInstancePerTest}

class AuthInterpreterSpec extends FlatSpec with OneInstancePerTest with MockFactory with Matchers {

  trait Fixture {
    implicit val authRepositoryMock = mock[AuthRepository]
    val interpreter: AuthAction ~> Id = AuthInterpreter.interpret[Id](catsInstancesForId, authRepositoryMock)
  }

  "AuthInterpreter" should "register a user" in {
    new Fixture {
      val user = User(
        PersonalData(
          "mauropalsgraaf@hotmail.com",
          "Mauro Palsgraaf",
          "Turfschip 16",
          "1111 XL",
          "Amstelveen"
        ),
        "password"
      )

      (authRepositoryMock.register _).expects(user).once().returning(
        Task.now {
          Right[Throwable, Unit](())
        }
      )

      assert(
        Auth.register(user).foldMap(interpreter) == Right(())
      )
    }
  }

  "AuthInterpreter" should "throw throwable authRepository cannot register user in" in {
    new Fixture() {
      val user = User(
        PersonalData(
          "mauropalsgraaf@hotmail.com",
          "Mauro Palsgraaf",
          "Turfschip 16",
          "1111 XL",
          "Amstelveen"
        ),
        "password"
      )

      val throwable = new Exception("I could not register")

      (authRepositoryMock.register _).expects(user).once().returning {
        Task.now {
          Left[Throwable, Unit](throwable)
        }
      }

      val result = Auth.register(user).foldMap(interpreter)

      result match {
        case Left(throwed) => assert(throwed == throwable)
        case Right(()) => fail()
      }
    }
  }


  "AuthInterpreter" should "validate client" in {
    new Fixture() {
      val client = Client("client-id", None)

      (authRepositoryMock.validateClient _).expects(client).once().returning {
        Task.now {
          true
        }
      }

      assert(Auth.validateClient(client).foldMap(interpreter))
    }
  }

  "AuthInterpreter" should "return false if authRepository returns false" in {
    new Fixture() {
      val client = Client("client-id", None)

      (authRepositoryMock.validateClient _).expects(client).once().returning {
        Task.now {
          false
        }
      }

      assert(!Auth.validateClient(client).foldMap(interpreter))
    }
  }

  "AuthInterpreter" should "return the user if found" in {
    new Fixture() {
      val email = "mauropalsgraaf@hotmail.com"
      val password = "supersocker"
      val hashedPassword = "2275e0012b69df190fb68a022ce4e9b677a0d0f46d1f42f6821c9a358c5ef0cc"
      val expectedUser = User(
        PersonalData(
          "mauropalsgraaf@hotmail.com",
          "Mauro Palsgraaf",
          "Turfschip 16",
          "1111 XL",
          "Amstelveen"
        ),
        "supersocker"
      )

      (authRepositoryMock.findUser _).expects(email, hashedPassword).once().returning {
        Task.now {
          Some(expectedUser)
        }
      }

      val result: Id[Option[User]] = Auth.findUser(email, password).foldMap(interpreter)

      result match {
        case Some(actualUser) => actualUser == expectedUser
        case None => fail()
      }
    }
  }

  "AuthInterpreter" should "return none is repository cannot find user" in {
    new Fixture() {
      val email = "mauropalsgraaf@hotmail.com"
      val password = "supersocker"
      val hashedPassword = "2275e0012b69df190fb68a022ce4e9b677a0d0f46d1f42f6821c9a358c5ef0cc"
      val user = User(
        PersonalData(
          email,
          "Mauro Palsgraaf",
          "Turfschip 16",
          "1111 XL",
          "Amstelveen"
        ),
        password
      )

      (authRepositoryMock.findUser _).expects(email, hashedPassword).once().returning {
        Task.now {
          None
        }
      }

      val result: Id[Option[User]] = Auth.findUser(email, password).foldMap(interpreter)

      assert(result.isEmpty)
    }
  }

  "AuthInterpreter" should "create a new access token for user" in {
    new Fixture() {
      val user = User(
        PersonalData(
          "mauropalsgraaf@hotmail.com",
          "Mauro Palsgraaf",
          "Turfschip 16",
          "1111 XL",
          "Amstelveen"
        ),
        "supersocker"
      )

      val clientId = "client-id"

      val result: Id[AccessToken] = Auth.createAccessToken(user, clientId).foldMap(interpreter)

      assert(result.user == user)
      assert(result.clientId == clientId)
    }
  }

  "AuthInterpreter" should "register access token" in {
    new Fixture() {
      val user = User(
        PersonalData(
          "mauropalsgraaf@hotmail.com",
          "Mauro Palsgraaf",
          "Turfschip 16",
          "1111 XL",
          "Amstelveen"
        ),
        "supersocker"
      )

      val accessToken = AccessToken(
        user,
        "clientId",
        "token",
        Instant.ofEpochSecond(1505207889)
      )

      (authRepositoryMock.saveAccessToken _).expects(accessToken).once().returning {
        Task.now {
          ()
        }
      }

      Auth.registerAccessToken(accessToken).foldMap(interpreter)
    }
  }

  "AuthInterpreter" should "find access token by user and client id" in {
    new Fixture() {
      val user = User(
        PersonalData(
          "mauropalsgraaf@hotmail.com",
          "Mauro Palsgraaf",
          "Turfschip 16",
          "1111 XL",
          "Amstelveen"
        ),
        "supersocker"
      )

      val clientId = "clientId"

      val accessToken = AccessToken(
        user,
        clientId,
        "token",
        Instant.now().minusSeconds(3200)
      )

      (authRepositoryMock.findAccessTokenByUserAndClientId _).expects(user.personalData.email, clientId).once().returning {
        Task.now {
          Some(accessToken)
        }
      }

      val maybeAccessToken: Option[AccessToken] = Auth.findAccessTokenByUserAndClientId(user, clientId).foldMap(interpreter)

      maybeAccessToken match {
        case Some(actualAccessToken) => actualAccessToken == accessToken
        case None => fail()
      }
    }
  }

  "AuthInterpreter" should "return none if access token is not found by user and client id" in {
    new Fixture() {
      val user = User(
        PersonalData(
          "mauropalsgraaf@hotmail.com",
          "Mauro Palsgraaf",
          "Turfschip 16",
          "1111 XL",
          "Amstelveen"
        ),
        "supersocker"
      )

      val clientId = "clientId"

      (authRepositoryMock.findAccessTokenByUserAndClientId _).expects(user.personalData.email, clientId).once().returning {
        Task.now { None }
      }

      val maybeAccessToken: Option[AccessToken] = Auth.findAccessTokenByUserAndClientId(user, clientId).foldMap(interpreter)

      assert(maybeAccessToken.isEmpty)
    }
  }

  "AuthInterpreter" should "find user and client id by access token" in {
    new Fixture() {
      val user = User(
        PersonalData(
          "mauropalsgraaf@hotmail.com",
          "Mauro Palsgraaf",
          "Turfschip 16",
          "1111 XL",
          "Amstelveen"
        ),
        "supersocker"
      )

      val clientId = "clientId"

      val accessToken = AccessToken(
        user,
        clientId,
        "token",
        Instant.ofEpochSecond(1505207889)
      )

      (authRepositoryMock.findUserAndClientIdByAccessToken _).expects(accessToken.token).once().returning {
        Task.now {
          Some {
            (user, clientId)
          }
        }
      }

      val maybeAccessToken: Option[(User, String)] = Auth.findUserAndClientIdByAccessToken(accessToken.token).foldMap(interpreter)

      maybeAccessToken match {
        case Some((actualUser, actualClientId)) => assert(user == actualUser && clientId == actualClientId)
        case None => fail()
      }
    }
  }

  "AuthInterpreter" should "return none user and client id are not found" in {
    new Fixture() {
      val clientId = "clientId"

      val user = User(
        PersonalData(
          "mauropalsgraaf@hotmail.com",
          "Mauro Palsgraaf",
          "Turfschip 16",
          "1111 XL",
          "Amstelveen"
        ),
        "password"
      )

      val accessToken = AccessToken(
        user,
        clientId,
        "token",
        Instant.ofEpochSecond(1505207889)
      )

      (authRepositoryMock.findUserAndClientIdByAccessToken _).expects(accessToken.token).once().returning {
        Task.now { None }
      }

      val maybeUserAndClientId: Option[(User, String)] = Auth.findUserAndClientIdByAccessToken(accessToken.token).foldMap(interpreter)

      assert(maybeUserAndClientId.isEmpty)
    }
  }

  "AuthInterpreter" should "find access token" in {
    new Fixture() {
      val user = User(
        PersonalData(
          "mauropalsgraaf@hotmail.com",
          "Mauro Palsgraaf",
          "Turfschip 16",
          "1111 XL",
          "Amstelveen"
        ),
        "supersocker"
      )

      val clientId = "clientId"

      val accessToken = AccessToken(
        user,
        clientId,
        "token",
        Instant.now().minusSeconds(3200) //400 seconds left before expiration
      )

      (authRepositoryMock.findAccessToken _).expects(accessToken.token).once().returning {
        Task.now {
          Some { accessToken }
        }
      }

      val maybeAccessToken: Option[AccessToken] = Auth.findAccessToken(accessToken.token).foldMap(interpreter)

      maybeAccessToken match {
        case Some(actualAccessToken) => assert(actualAccessToken == accessToken)
        case None => fail()
      }
    }
  }

  "AuthInterpreter" should "return none if access token is not found" in {
    new Fixture() {
      val clientId = "clientId"

      val user = User(
        PersonalData(
          "mauropalsgraaf@hotmail.com",
          "Mauro Palsgraaf",
          "Turfschip 16",
          "1111 XL",
          "Amstelveen"
        ),
        "password"
      )

      val accessToken = AccessToken(
        user,
        clientId,
        "token",
        Instant.now()
      )

      (authRepositoryMock.findAccessToken _).expects(accessToken.token).once().returning {
        Task.now { None }
      }

      val maybeUserAndClientId: Option[AccessToken] = Auth.findAccessToken(accessToken.token).foldMap(interpreter)

      assert(maybeUserAndClientId.isEmpty)
    }
  }

  "AuthInterpreter" should "return none if access token is found but expired" in {
    new Fixture() {
      val clientId = "clientId"

      val user = User(
        PersonalData(
          "mauropalsgraaf@hotmail.com",
          "Mauro Palsgraaf",
          "Turfschip 16",
          "1111 XL",
          "Amstelveen"
        ),
        "password"
      )

      val accessToken = AccessToken(
        user,
        clientId,
        "token",
        Instant.now().minusSeconds(3601)
      )

      (authRepositoryMock.findAccessToken _).expects(accessToken.token).once().returning {
        Task.now { Some(accessToken) }
      }

      val maybeUserAndClientId: Option[AccessToken] = Auth.findAccessToken(accessToken.token).foldMap(interpreter)

      assert(maybeUserAndClientId.isEmpty)
    }
  }

  "AuthInterpreter" should "logout a user" in {
    new Fixture() {
      val clientId = "clientId"

      val user = User(
        PersonalData(
          "mauropalsgraaf@hotmail.com",
          "Mauro Palsgraaf",
          "Turfschip 16",
          "1111 XL",
          "Amstelveen"
        ),
        "password"
      )

      (authRepositoryMock.deleteAccessToken _).expects(user.personalData.email, clientId).once().returning {
        Task.now { () }
      }

      Auth.logout(user.personalData.email, clientId).foldMap(interpreter)
    }
  }
}