package com.petsupplies.authenticationservice.persistence

import com.petsupplies.authenticationservice.types.Client.Client
import doobie.scalatest.QueryChecker
import doobie.util.iolite.IOLite
import doobie.util.transactor.Transactor
import org.scalatest.{FunSuite, Matchers}
import com.petsupplies.authenticationservice.implicits.doobieAuthQuery

class AuthQuerySpec extends FunSuite with Matchers with QueryChecker {
  def transactor: Transactor[IOLite] = TestTransactor.transactor

  test("doesClientExist") { checkOutput(doobieAuthQuery.doesClientExist(Client("", None)))}
  test("findUser") { checkOutput(doobieAuthQuery.findUser("", "")) }
  test("findAccessTokenByUserAndClientId") { checkOutput(doobieAuthQuery.findAccessTokenByUserAndClientId("", "")) }
  test("findUserAndClientIdByAccessToken") { checkOutput(doobieAuthQuery.findUserAndClientIdByAccessToken("")) }
  test("findAccessToken") { checkOutput(doobieAuthQuery.findAccessToken("")) }
}
