package com.petsupplies.authenticationservice

import com.petsupplies.authenticationservice.interpreter.PureIdAuthInterpreter
import com.petsupplies.authenticationservice.interpreter.AuthInterpreterTestData._
import com.petsupplies.authenticationservice.types.AccessToken
import com.petsupplies.authenticationservice.types.Client.Client
import com.petsupplies.authenticationservice.types.User.User
import com.petsupplies.authenticationservice.types.PersonalData.PersonalData
import org.scalatest.{FlatSpec, Matchers}

class AuthServiceSpec extends FlatSpec with Matchers {
  "The authentication service" should "successfully register a user" in {
    val user = User(
      PersonalData("email@hotmail.com", "full name", "address 12", "1192 XL", "residence"),
      "password"
    )
    val program = AuthService.register(user)

    val result = program.foldMap(PureIdAuthInterpreter.interpret)

    assert(result.isRight === true)
  }

  "The authentication service" should "return true if client is not valid" in {
    val program = AuthService.validateClient(Client(dummyCorrectClientId, None))

    val result = program.foldMap(PureIdAuthInterpreter.interpret)

    assert(result)
  }

  "The authentication service" should "return false if client is not valid" in {
    val program = AuthService.validateClient(Client("not valid client id", None))

    val result = program.foldMap(PureIdAuthInterpreter.interpret)

    assert(!result)
  }

  "The authentication service" should "find user given an email and password" in {
    val program = AuthService.findUser(dummyUser.personalData.email, dummyUser.password)

    val result = program.foldMap(PureIdAuthInterpreter.interpret)

    result match {
      case Some(user) => assert(user == dummyUser)
      case None => fail()
    }
  }

  "The authentication service" should "create an access token" in {
    val program = AuthService.createAccessToken(dummyUser, dummyCorrectClientId)

    val result = program.foldMap(PureIdAuthInterpreter.interpret)

    assert(
      result == AccessToken(
        dummyUser,
        dummyCorrectClientId,
        dummyCorrectAccessToken,
        dummyInstant
      )
    )
  }

  "The authentication service" should "find access token by user and client id" in {
    val program = AuthService.findAccessTokenByUserAndClientId(dummyUser, dummyCorrectClientId)

    val result = program.foldMap(PureIdAuthInterpreter.interpret)

    assert(
      result.get == AccessToken(
        dummyUser,
        dummyCorrectClientId,
        dummyCorrectAccessToken,
        dummyInstant
      )
    )
  }

  "The authentication service" should "find user and client id by access token" in {
    val program = AuthService.findUserAndClientIdByAccessToken(dummyCorrectAccessToken)

    val result = program.foldMap(PureIdAuthInterpreter.interpret)

    result match {
      case Some((user, clientId)) => assert(
        user == dummyUser && clientId == dummyCorrectClientId
      )
      case None => fail()
    }
  }

  "The authentication service" should "find access token" in {
    val program = AuthService.findAccessToken(dummyCorrectAccessToken)

    val result = program.foldMap(PureIdAuthInterpreter.interpret)

    assert(
      result.get == AccessToken(
        dummyUser,
        dummyCorrectClientId,
        dummyCorrectAccessToken,
        dummyInstant
      )
    )
  }

  "The authentication service" should "logout user" in {
    val program = AuthService.logout(dummyUser.personalData.email, dummyCorrectClientId)

    program.foldMap(PureIdAuthInterpreter.interpret)
  }
}
