package com.petsupplies.authenticationservice.types

import cats.data.Validated.{Invalid, Valid}
import com.petsupplies.authenticationservice.types.User.{User => DomainUser}
import com.petsupplies.authenticationservice.generators.UserGen._
import com.petsupplies.authenticationservice.util.validation.ValidationFailed
import org.scalacheck.Prop.forAll
import org.scalacheck.Properties
import org.scalatest.Matchers

object UserProp extends Properties("User validation") with Matchers {

  property("should allow valid password") = forAll(validPasswordGen) {
    password: String =>
      User.validatePassword(password) match {
        case Valid(validatedPassword) => validatedPassword == password
        case Invalid(_) => fail()
      }
  }

  property("should not allow passwords shorter than 6 characters") = forAll(invalidPasswordGen) {
    password: String =>
      User.validatePassword(password) match {
        case Valid(_) => fail()
        case Invalid(x) => x.head match {
          case ValidationFailed(validationFailedException) => validationFailedException == "Vul een geldig wachtwoord in. Een geldig wachtwoord bestaat uit minimaal 6 karakters"
        }
      }
  }

  property("Should allow valid user") = forAll(validUserGen) {
    user: DomainUser =>
      User.validate(user) match {
        case Valid(validatedUser) =>
          user == validatedUser
        case Invalid(_) => fail()
      }
  }
}
