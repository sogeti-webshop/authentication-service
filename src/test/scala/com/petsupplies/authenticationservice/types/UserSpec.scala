package com.petsupplies.authenticationservice.types

import com.petsupplies.authenticationservice.util.crypto.Sha256
import org.scalatest.{FlatSpec, Matchers}

class UserSpec extends FlatSpec with Matchers {
  "User" should "be instantiatable" in {
    val user = User.User(
      PersonalData.PersonalData("email@hotmail.com", "full name", "address 12", "1192 XL", "residence"),
      "password"
    )

      assert(user.personalData.email === "email@hotmail.com")
    assert(user.password === "password")
    assert(user.personalData.fullName === "full name")
    assert(user.personalData.address === "address 12")
    assert(user.personalData.postalCode === "1192 XL")
    assert(user.personalData.residence === "residence")
  }

  "Users password" should "be hashed without changing other information" in {
    val user = User.User(
      PersonalData.PersonalData("email@hotmail.com", "full name", "address 12", "1192 XL", "residence"),
      "password"
    )

    val userWithHashedPassword = User.hashPassword(user)

    assert(user.personalData.email === userWithHashedPassword.personalData.email)
    assert(Sha256.hash(user.password) === userWithHashedPassword.password)
    assert(user.personalData.fullName === userWithHashedPassword.personalData.fullName)
    assert(user.personalData.address === userWithHashedPassword.personalData.address)
    assert(user.personalData.postalCode === userWithHashedPassword.personalData.postalCode)
    assert(user.personalData.residence === userWithHashedPassword.personalData.residence)
  }
}
