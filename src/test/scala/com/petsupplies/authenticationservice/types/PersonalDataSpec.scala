package com.petsupplies.authenticationservice.types

import cats.data.Validated.{Invalid, Valid}
import com.petsupplies.authenticationservice.util.validation.ValidationFailed
import org.scalatest.{FlatSpec, Matchers}

class PersonalDataSpec extends FlatSpec with Matchers {
  "User validation" should "not allow empty full name" in {
    PersonalData.validateFullName("") match {
      case Valid(_) => fail()
      case Invalid(x) => x.head match {
        case ValidationFailed(validationFailedException) =>
          assert(validationFailedException === "Vul een naam in")
      }
    }
  }

  it should "not allow empty address" in {
    PersonalData.validateAddress("") match {
      case Valid(_) => fail()
      case Invalid(x) => x.head match {
        case ValidationFailed(validationFailedException) =>
          assert(validationFailedException === "Vul een adres in")
      }
    }
  }

  it should "not allow empty residence" in {
    PersonalData.validateResidence("") match {
      case Valid(_) => fail()
      case Invalid(x) => x.head match {
        case ValidationFailed(validationFailedException) =>
          assert(validationFailedException === "Vul een woonplaats in")
      }
    }
  }
}
