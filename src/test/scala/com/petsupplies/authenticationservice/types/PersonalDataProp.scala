package com.petsupplies.authenticationservice.types

import cats.data.Validated.{Invalid, Valid}
import com.petsupplies.authenticationservice.generators.UserGen._
import com.petsupplies.authenticationservice.types.UserProp.{fail, property}
import com.petsupplies.authenticationservice.util.validation.ValidationFailed
import org.scalacheck.Prop.forAll
import org.scalacheck.Properties

object PersonalDataProp extends Properties("PersonalData") {

  property("should allow valid email") = forAll(validEmailGen) {
    email: String =>
      PersonalData.validateEmail(email) match {
        case Valid(validatedEmail) => validatedEmail == email
        case Invalid(_) => fail()
      }
  }

  property("should not allow invalid email") = forAll(invalidEmailGen) {
    email: String =>
      PersonalData.validateEmail(email) match {
        case Valid(_) => fail()
        case Invalid(x) => x.head match {
          case ValidationFailed(validationFailedException) => validationFailedException == "Vul een geldig e-mailadres in"
        }
      }
  }

  property("should allow valid fullName") = forAll(validFullNameGen) {
    fullName: String =>
      PersonalData.validateFullName(fullName) match {
        case Valid(validatedFullName) => validatedFullName == fullName
        case Invalid(_) => fail()
      }
  }

  property("should allow valid address") = forAll(validAddressGen) {
    address: String =>
      PersonalData.validateAddress(address) match {
        case Valid(validatedAddress) => validatedAddress == address
        case Invalid(_) => fail()
      }
  }

  property("should allow valid postalCode") = forAll(validPostalCodeGen) {
    postalCode: String =>
      PersonalData.validatePostalCode(postalCode) match {
        case Valid(validatedPostalCode) => validatedPostalCode == postalCode
        case Invalid(_) => fail()
      }
  }

  property("should not allow invalid postalCode of different format than 9999 BA (space is optional)") = forAll(invalidPostalCode) {
    postalCode: String =>
      PersonalData.validatePostalCode(postalCode) match {
        case Valid(_) => fail()
        case Invalid(x) => x.head match {
          case ValidationFailed(validationFailedException) =>
            validationFailedException == "Vul een geldige postcode in (voorbeeld: 9999 BA)"
        }
      }
  }

  property("should allow valid residence") = forAll(validResidenceGen) {
    residence: String =>
      PersonalData.validateResidence(residence) match {
        case Valid(validatedResidence) => validatedResidence == residence
        case Invalid(_) => fail()
      }
  }

  property("should allow valid personalData") = forAll(validPersonalDataGen) {
    personalData: PersonalData.PersonalData =>
      PersonalData.validatePersonalData(personalData) match {
        case Valid(validatedPersonalData) => validatedPersonalData == personalData
        case Invalid(_) => fail()
      }
  }
}
