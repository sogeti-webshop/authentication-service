package com.petsupplies.authenticationservice

import doobie.imports.{DriverManagerTransactor, Transactor}
import fs2.Task
import com.petsupplies.authenticationservice.persistence.{AuthQueryInstances, AuthRepositoryInstances}

class ImplicitsInstances extends AuthRepositoryInstances
    with AuthQueryInstances {
  // TechDebt: These values should come from environment variable to make it easy to deploy over multiple environments
  implicit val transactor: Transactor[Task] =
    DriverManagerTransactor[Task]("com.mysql.jdbc.Driver", "jdbc:mysql://mysql:3306/petsupplies", "root", "")
}

object implicits extends ImplicitsInstances
