package com.petsupplies.authenticationservice.persistence

import com.petsupplies.authenticationservice.types.AccessToken
import com.petsupplies.authenticationservice.types.Client.{ Client, ClientId }
import com.petsupplies.authenticationservice.types.User.{Password, User}
import com.petsupplies.authenticationservice.types.PersonalData.Email
import doobie.imports.{Fragment, Query0, Update0, toSqlInterpolator}
import doobie.util.fragments.whereAndOpt

// This interface is not great, since it depends on doobie return types. It really has no added value beyond
// having the possibility to create a mock or stub for the authRepository and increase testability
trait AuthQuery {
  def add(u: User): Update0
  def doesClientExist(client: Client): Query0[Long]
  def findUser(email: Email, password: Password): Query0[User]
  def saveAccessToken(at: AccessToken): Update0
  def findAccessTokenByUserAndClientId(email: Email, clientId: ClientId): Query0[AccessToken]
  def findUserAndClientIdByAccessToken(token: String): Query0[(User, ClientId)]
  def findAccessToken(token: String): Query0[AccessToken]
  def deleteAccessToken(email: Email, clientId: ClientId): Update0
}

trait AuthQueryInstances {
  implicit val doobieAuthQuery: AuthQuery = new AuthQuery {
    def add(u: User): Update0 = {
      val query: Fragment = sql"insert into users (email, password, full_name, address, postal_code, residence) values (${u.personalData.email}, ${u.password}, ${u.personalData.fullName}, ${u.personalData.address}, ${u.personalData.postalCode}, ${u.personalData.residence})"

      query.update
    }

    def doesClientExist(client: Client): Query0[Long] = {
      val clientIdFragment: Option[Fragment] = Some(fr"client_id = ${client.clientId}")
      val clientSecretFragment: Option[Fragment] = client.clientSecret.map(secret => fr"client_secret = $secret")

      val selectFragment: Fragment = fr"select 1 from clients"

      (selectFragment ++ whereAndOpt(clientIdFragment, clientSecretFragment))
          .query[Long] // There is not something to represent booleans, long is the recommended type by doobie.
    }

    def findUser(email: Email, password: Password): Query0[User] =
      sql"select email, full_name, address, postal_code, residence, password from users where email = $email and password = $password"
          .query[User]

    def saveAccessToken(at: AccessToken): Update0 = {
      sql"insert into access_tokens(email, client_id, access_token, creation_date) values (${at.user.personalData.email}, ${at.clientId}, ${at.token}, ${at.creationDate})"
          .update
    }

    def findAccessTokenByUserAndClientId(email: Email, clientId: ClientId): Query0[AccessToken] = {
      val select: Fragment = fr"select access_tokens.email, users.full_name, users.address, users.postal_code, users.residence, users.password, client_id, access_token, creation_date"
      val from: Fragment = fr"from users inner join access_tokens on users.email = access_tokens.email"
      val where: Fragment = fr"where access_tokens.email = $email and client_id = $clientId"

      (select ++ from ++ where ++ fr"ORDER BY access_tokens.creation_date DESC LIMIT 1")
          .query[AccessToken]
    }

    def findUserAndClientIdByAccessToken(token: String): Query0[(User, ClientId)] =
      sql"select access_tokens.email, full_name, address, postal_code, residence, password, client_id from access_tokens inner join users on access_tokens.email = users.email where access_token = $token"
          .query[(User, ClientId)]

    def findAccessToken(token: String): Query0[AccessToken] =
      sql"select access_tokens.email, full_name, address, postal_code, residence, password, client_id, access_token, creation_date from access_tokens inner join users on access_tokens.email = users.email where access_token = $token"
          .query[AccessToken]

    def deleteAccessToken(email: Email, clientId: ClientId): Update0 =
      sql"delete from access_tokens where email = $email and client_id = $clientId"
          .update
  }
}
