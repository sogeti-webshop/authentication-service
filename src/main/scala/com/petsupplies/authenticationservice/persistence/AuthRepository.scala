package com.petsupplies.authenticationservice.persistence

import com.petsupplies.authenticationservice.types.AccessToken
import com.petsupplies.authenticationservice.types.Client.{Client, ClientId}
import com.petsupplies.authenticationservice.types.User.{Password, User}
import com.petsupplies.authenticationservice.types.PersonalData.Email
import doobie.imports._
import fs2.Task

trait AuthRepository {
  def register(user: User): Task[Either[Throwable, Unit]]

  def validateClient(client: Client): Task[Boolean]

  def findUser(email: Email, password: Password): Task[Option[User]]

  def saveAccessToken(accessToken: AccessToken): Task[Unit]

  def findAccessTokenByUserAndClientId(email: Email, clientId: ClientId): Task[Option[AccessToken]]

  def findUserAndClientIdByAccessToken(token: String): Task[Option[(User, ClientId)]]

  def findAccessToken(token: String): Task[Option[AccessToken]]
  
  def deleteAccessToken(email: Email, clientId: ClientId): Task[Unit]
}

trait AuthRepositoryInstances {
  implicit def doobieAuthRepository(implicit authQuery: AuthQuery, transactor: Transactor[Task]): AuthRepository = new AuthRepository {
    def register(user: User): Task[Either[Throwable, Unit]] =
      authQuery.add(user)
          .run
          .map(_ => ())
          .transact(transactor)
          .attempt

    def validateClient(client: Client): Task[Boolean] =
      authQuery.doesClientExist(client)
          .option
          .map {
            case Some(_) => true
            case None => false
          }
          .transact(transactor)

    def findUser(email: Email, password: Password): Task[Option[User]] =
      authQuery.findUser(email, password)
          .option
          .transact(transactor)

    def saveAccessToken(accessToken: AccessToken): Task[Unit] =
      authQuery.saveAccessToken(accessToken)
          .run
          .map(_ => ())
          .transact(transactor)

    def findAccessTokenByUserAndClientId(email: Email, clientId: ClientId): Task[Option[AccessToken]] =
      authQuery.findAccessTokenByUserAndClientId(email, clientId)
          .option
          .transact(transactor)

    def findUserAndClientIdByAccessToken(token: String): Task[Option[(User, ClientId)]] =
      authQuery.findUserAndClientIdByAccessToken(token)
          .option
          .transact(transactor)

    def findAccessToken(token: String): Task[Option[AccessToken]] =
      authQuery.findAccessToken(token)
          .option
          .transact(transactor)

    def deleteAccessToken(email: Email, clientId: ClientId): Task[Unit] =
      authQuery.deleteAccessToken(email, clientId)
          .run
          .transact(transactor)
          .map(_ => ())
  }
}