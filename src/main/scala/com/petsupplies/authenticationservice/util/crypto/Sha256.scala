package com.petsupplies.authenticationservice.util.crypto

import com.roundeights.hasher.Implicits._

private[authenticationservice] object Sha256 {
  def hash(value: String): String = value.sha256.hex
}
