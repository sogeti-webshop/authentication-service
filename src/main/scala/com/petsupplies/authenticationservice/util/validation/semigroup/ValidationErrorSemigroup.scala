package com.petsupplies.authenticationservice.util.validation.semigroup

import cats.Semigroup
import com.petsupplies.authenticationservice.util.validation.{ValidationError, ValidationFailed}

object ValidationErrorSemigroup {
  implicit def validationErrorSemigroup = new Semigroup[ValidationError] {
    def combine(x: ValidationError, y: ValidationError): ValidationError = (x, y) match {
      case (ValidationFailed(mes1), ValidationFailed(mes2)) => ValidationFailed(
        s"$mes1,$mes2"
      )
    }
  }
}
