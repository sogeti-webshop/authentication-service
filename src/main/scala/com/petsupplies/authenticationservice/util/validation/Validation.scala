package com.petsupplies.authenticationservice.util.validation

import cats.data.Validated.{Invalid, Valid}
import cats.data.ValidatedNel

sealed trait ValidationError
case class ValidationFailed(message: String) extends ValidationError

object Validation {
  type Validation[A] = ValidatedNel[ValidationError, A]

  def minLength(value: String, minLength: Int)(message: String): Validation[String] =
    if (value.length >= minLength) { Valid(value) }
    else { Invalid(ValidationFailed(message)).toValidatedNel }

  def inBetweenLength(value: String, minLength: Int, maxLength: Int)(message: String): Validation[String] =
    if (value.length >= minLength && value.length <= maxLength) { Valid(value) }
    else { Invalid(ValidationFailed(message)).toValidatedNel }

  def nonEmpty(value: String)(message: String): Validation[String] =
    minLength(value, 1)(message)

  def regex(value: String, regex: String)(message: String): Validation[String] =
    if (value.matches(regex)) { Valid(value) }
    else { Invalid(ValidationFailed(message)).toValidatedNel }
}
