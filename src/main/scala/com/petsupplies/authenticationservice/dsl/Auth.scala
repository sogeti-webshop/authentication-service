package com.petsupplies.authenticationservice.dsl

import cats.free.Free
import com.petsupplies.authenticationservice.dsl.AuthAction._
import com.petsupplies.authenticationservice.types.AccessToken
import com.petsupplies.authenticationservice.types.Client.{Client, ClientId}
import com.petsupplies.authenticationservice.types.User.{Password, User}
import com.petsupplies.authenticationservice.types.PersonalData.Email

object Auth {
  type AuthF[A] = Free[AuthAction, A]

  def register(user: User): AuthF[Either[Throwable, Unit]] =
    Free.liftF[AuthAction, Either[Throwable, Unit]](Register(user))

  def validateClient(client: Client): AuthF[Boolean] =
    Free.liftF[AuthAction, Boolean](ValidateClient(client))

  def findUser(email: Email, password: Password): AuthF[Option[User]] =
    Free.liftF[AuthAction, Option[User]](FindUser(email, password))

  def createAccessToken(user: User, clientId: ClientId): AuthF[AccessToken] =
    Free.liftF[AuthAction, AccessToken](CreateAccessToken(user, clientId))

  def registerAccessToken(accessToken: AccessToken): AuthF[Unit] =
    Free.liftF[AuthAction, Unit](RegisterAccessToken(accessToken))

  def findAccessTokenByUserAndClientId(user: User, clientId: ClientId): AuthF[Option[AccessToken]] =
    Free.liftF[AuthAction, Option[AccessToken]](FindAccessTokenByUserAndClientId(user, clientId))

  def findUserAndClientIdByAccessToken(accessToken: String): AuthF[Option[(User, String)]] =
    Free.liftF[AuthAction, Option[(User, String)]](FindUserAndClientIdByAccessToken(accessToken))

  def findAccessToken(token: String): AuthF[Option[AccessToken]] =
    Free.liftF[AuthAction, Option[AccessToken]](FindAccessToken(token))

  def logout(email: Email, clientId: ClientId): AuthF[Unit] =
    Free.liftF(Logout(email, clientId))
}
