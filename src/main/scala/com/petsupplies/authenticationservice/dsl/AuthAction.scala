package com.petsupplies.authenticationservice.dsl

import com.petsupplies.authenticationservice.types.AccessToken
import com.petsupplies.authenticationservice.types.Client.{Client, ClientId}
import com.petsupplies.authenticationservice.types.User.{Password, User}
import com.petsupplies.authenticationservice.types.PersonalData.Email

sealed trait AuthAction[T]

object AuthAction {
  case class Register(user: User) extends AuthAction[Either[Throwable, Unit]]
  case class ValidateClient(client: Client) extends AuthAction[Boolean]
  case class FindUser(email: Email, password: Password) extends AuthAction[Option[User]]
  case class CreateAccessToken(user: User, clientId: ClientId) extends AuthAction[AccessToken]
  case class RegisterAccessToken(accessToken: AccessToken) extends AuthAction[Unit]
  case class FindAccessTokenByUserAndClientId(user: User, clientId: ClientId) extends AuthAction[Option[AccessToken]]
  case class FindUserAndClientIdByAccessToken(token: String) extends AuthAction[Option[(User, ClientId)]]
  case class FindAccessToken(token: String) extends AuthAction[Option[AccessToken]]
  case class Logout(email: Email, clientId: ClientId) extends AuthAction[Unit]
}
