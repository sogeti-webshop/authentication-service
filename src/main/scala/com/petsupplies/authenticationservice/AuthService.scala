package com.petsupplies.authenticationservice

import com.petsupplies.authenticationservice.dsl.Auth
import com.petsupplies.authenticationservice.dsl.Auth.AuthF
import com.petsupplies.authenticationservice.types.AccessToken
import com.petsupplies.authenticationservice.types.Client.{Client, ClientId}
import com.petsupplies.authenticationservice.types.PersonalData.Email
import com.petsupplies.authenticationservice.types.User.{Password, User}

object AuthService {
  def register(user: User): AuthF[Either[Throwable, Unit]] = Auth.register(user)

  def validateClient(client: Client): AuthF[Boolean] = Auth.validateClient(client)

  def findUser(email: Email, password: Password): AuthF[Option[User]] = Auth.findUser(email, password)

  def createAccessToken(user: User, clientId: ClientId): AuthF[AccessToken] = for {
    accessToken <- Auth.createAccessToken(user, clientId)
    _ <- Auth.registerAccessToken(accessToken)
  } yield accessToken

  def findAccessTokenByUserAndClientId(user: User, clientId: ClientId): AuthF[Option[AccessToken]] =
    Auth.findAccessTokenByUserAndClientId(user, clientId)

  def findUserAndClientIdByAccessToken(accessToken: String): AuthF[Option[(User, String)]] =
    Auth.findUserAndClientIdByAccessToken(accessToken)

  def findAccessToken(token: String): AuthF[Option[AccessToken]] =
    Auth.findAccessToken(token)

  def logout(email: Email, clientId: ClientId): AuthF[Unit] =
    Auth.logout(email, clientId)
}
