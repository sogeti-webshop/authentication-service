package com.petsupplies.authenticationservice.types

import cats.Apply
import com.petsupplies.authenticationservice.util.validation.Validation
import com.petsupplies.authenticationservice.util.validation.Validation.Validation

object PersonalData {
  type Email = String
  type FullName = String
  type Address = String
  type PostalCode = String
  type Residence = String

  case class PersonalData(
    email: Email,
    fullName: FullName,
    address: Address,
    postalCode: PostalCode,
    residence: Residence
  )

  val emailRegex = "^(([^<>()\\[\\]\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"
  val postalCodeRegex = "^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-zA-Z]{2}$"

  def validateEmail(email: String): Validation[Email] = {
    Validation.regex(email, emailRegex)("Vul een geldig e-mailadres in")
  }

  def validateFullName(fullName: String): Validation[FullName] =
    Validation.nonEmpty(fullName)("Vul een naam in")

  def validateAddress(address: String): Validation[Address] =
    Validation.nonEmpty(address)("Vul een adres in")

  def validatePostalCode(postalCode: String): Validation[PostalCode] =
    Validation.regex(postalCode, postalCodeRegex)("Vul een geldige postcode in (voorbeeld: 9999 BA)")

  def validateResidence(residence: String): Validation[Residence] =
    Validation.nonEmpty(residence)("Vul een woonplaats in")


  def validatePersonalData(personalData: PersonalData): Validation[PersonalData] = {
    Apply[Validation].map5(
      validateEmail(personalData.email),
      validateFullName(personalData.fullName),
      validateAddress(personalData.address),
      validatePostalCode(personalData.postalCode),
      validateResidence(personalData.residence)
    )(PersonalData.apply)
  }
}
