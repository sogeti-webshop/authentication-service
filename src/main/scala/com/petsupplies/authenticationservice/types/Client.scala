package com.petsupplies.authenticationservice.types

object Client {
  case class Client(clientId: ClientId, clientSecret: Option[ClientSecret])

  type ClientId = String
  type ClientSecret = String
  type GrantType = String
}
