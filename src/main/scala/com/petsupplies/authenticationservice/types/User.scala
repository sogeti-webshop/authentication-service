package com.petsupplies.authenticationservice.types

import cats.Apply
import com.petsupplies.authenticationservice.util.crypto.Sha256
import com.petsupplies.authenticationservice.util.validation.Validation
import com.petsupplies.authenticationservice.util.validation.Validation.Validation

object User {
  type Password = String

  case class User(
    personalData: PersonalData.PersonalData,
    password: Password
  )

  def validatePassword(password: String): Validation[Password] = {
    val minPasswordLength = 6
    Validation.minLength(password, minPasswordLength)(s"Vul een geldig wachtwoord in. Een geldig wachtwoord bestaat uit minimaal $minPasswordLength karakters")
  }

  def validate(user: User): Validation[User] =
    Apply[Validation].map2(
      PersonalData.validatePersonalData(user.personalData),
      validatePassword(user.password)
    )(User.apply)

  def hashPassword(user: User): User = user.copy(password = Sha256.hash(user.password))
}
