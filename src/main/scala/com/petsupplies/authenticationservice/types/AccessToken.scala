package com.petsupplies.authenticationservice.types

import java.time.Instant
import com.petsupplies.authenticationservice.types.Client.ClientId
import com.petsupplies.authenticationservice.types.User.{User => DomainUser}

case class AccessToken(user: DomainUser, clientId: ClientId, token: String, creationDate: Instant)
