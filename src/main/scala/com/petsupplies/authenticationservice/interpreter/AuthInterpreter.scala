package com.petsupplies.authenticationservice.interpreter

import java.time.Instant
import java.util.UUID

import cats.{Monad, ~>}
import com.petsupplies.authenticationservice.dsl.AuthAction
import com.petsupplies.authenticationservice.dsl.AuthAction._
import com.petsupplies.authenticationservice.persistence.AuthRepository
import com.petsupplies.authenticationservice.types.AccessToken
import com.petsupplies.authenticationservice.types.Client.ClientId
import com.petsupplies.authenticationservice.types.User.User
import com.petsupplies.authenticationservice.util.crypto.Sha256
import fs2.Task
import fs2.Strategy

import scala.concurrent.ExecutionContext.Implicits.global

object AuthInterpreter {
  implicit val strategy: Strategy = Strategy.fromExecutionContext(global)

  def interpret[M[_]: Monad](implicit authRepository: AuthRepository): AuthAction ~> M =
    new (AuthAction ~> M) {
      def apply[A](fa: AuthAction[A]): M[A] =
        fa match {
          case Register(user) =>
            Monad[M].pure {
              authRepository.register(user).unsafeRun()
            }

          case ValidateClient(client) =>
            Monad[M].pure {
              authRepository.validateClient(client).unsafeRun()
            }

          case FindUser(email, password) =>
            Monad[M].pure {
              authRepository.findUser(email, Sha256.hash(password)).unsafeRun()
            }

          case CreateAccessToken(user: User, clientId: ClientId) =>
            Monad[M].pure {
              val token = Sha256.hash(UUID.randomUUID().toString ++ user.personalData.email ++ clientId)
              AccessToken(user, clientId, token, Instant.now())
            }


          case RegisterAccessToken(accessToken: AccessToken) =>
            Monad[M].pure {
              authRepository.saveAccessToken(accessToken).unsafeRun()
            }

          case FindAccessTokenByUserAndClientId(user, clientId) =>
            Monad[M].pure {
              authRepository.findAccessTokenByUserAndClientId(user.personalData.email, clientId).unsafeRun()
                  .filter(
                    (accessToken) => isAccessTokenExpired(accessToken).unsafeRun()
                  )
            }

          case FindUserAndClientIdByAccessToken(token) =>
            Monad[M].pure {
              authRepository.findUserAndClientIdByAccessToken(token).unsafeRun()
            }

          case FindAccessToken(token) =>
            Monad[M].pure {
              authRepository.findAccessToken(token).unsafeRun()
                  .filter(
                    (accessToken) => isAccessTokenExpired(accessToken).unsafeRun()
                  )
            }

          case Logout(email, clientId) =>
            Monad[M].pure {
              authRepository.deleteAccessToken(email, clientId).unsafeRun()
            }
        }
    }

  def isAccessTokenExpired(accessToken: AccessToken): Task[Boolean] = {
    Task {
      val now: Instant = Instant.now()
      val tokenLifetime: Int = 3600 // lifetime of token
      accessToken.creationDate.plusSeconds(tokenLifetime).isAfter(now)
    }
  }
}
